package main

import (
	ti "bitbucket.org/vakoto/grpc-go-client"
	"context"
	"log"
	"time"

	"google.golang.org/grpc"
)

const (
	address = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(address, grpc.WithInsecure())
	if err != nil {
		log.Fatalf("did not connect: %v", err)
	}
	defer conn.Close()
	c := ti.NewTriangleInfoClient(conn)
	ctx, cancel := context.WithTimeout(context.Background(), time.Second)
	defer cancel()
	r, err := c.GetTriangleInfo(ctx, &ti.TriangleInfoRequest{
		Triangle: &ti.Triangle{
			P1: &ti.Point{ X: 0, Y: 0 },
			P2: &ti.Point{ X: 1, Y: 0 },
			P3: &ti.Point{ X: 0, Y: 1 },
		},
	})
	if err != nil {
		log.Fatalf("could not get reply: %v", err)
	}
	log.Printf("perimeter: %f\tarea: %f", r.Perimeter, r.Area)
}